package com.chang.easyexceldemo;

import lombok.Data;


@Data
public class DemoData {
    private String fullName;
    private String shortName;
    private String bankName;
    private String area;
}
