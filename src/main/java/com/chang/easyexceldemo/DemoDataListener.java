package com.chang.easyexceldemo;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

// 有个很重要的点 DemoDataListener 不能被spring管理，要每次读取excel都要new,然后里面用到spring可以构造方法传进去
public class DemoDataListener extends AnalysisEventListener<DemoData> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoDataListener.class);
    /**
     * 每隔5条存储数据库，实际使用中可以3000条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 1000;
    List<DemoData> list = new ArrayList<DemoData>();
    /**
     * 假设这个是一个DAO，当然有业务逻辑这个也可以是一个service。当然如果不用存储这个对象没用。
     */
//    private DemoDAO demoDAO;
//    public DemoDataListener() {
//        // 这里是demo，所以随便new一个。实际使用如果到了spring,请使用下面的有参构造函数
//        demoDAO = new DemoDAO();
//    }
    /**
     * 如果使用了spring,请使用这个构造方法。每次创建Listener的时候需要把spring管理的类传进来
     *
     * @param demoDAO
     */
//    public DemoDataListener(DemoDAO demoDAO) {
//        this.demoDAO = demoDAO;
//    }
    /**
     * 这个每一条数据解析都会来调用
     *
     * @param data
     *            one row value. Is is same as {@link AnalysisContext#readRowHolder()}
     * @param context
     */
    @Override
    public void invoke(DemoData data, AnalysisContext context) {
        LOGGER.info("解析到一条数据:{}", JSON.toJSONString(data));
        list.add(data);
        // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
        if (list.size() >= BATCH_COUNT) {
            saveData();
            // 存储完成清理 list
            list.clear();
        }
    }

    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        saveData();
        LOGGER.info("所有数据解析完成！");
    }
    /**
     * 加上存储数据库
     */
    private void saveData() {
        AtomicInteger id = new AtomicInteger(1);
        LOGGER.info("{}条数据，开始存储数据库！", list.size());
        // 1. 获取全部的所属银行
        List<DemoData> bankList = list
                .stream()
                .filter(distinctByKey(DemoData::getBankName))
                .collect(Collectors.toList());

        List<FormBank> parentBanks = new ArrayList<>();
        bankList.forEach(b -> {
            FormBank formBank = new FormBank();
            formBank.setId(id.getAndIncrement());
            formBank.setPid(0);
            formBank.setArea(b.getArea());
            formBank.setName(b.getBankName());
            parentBanks.add(formBank);
        });

        Map<String, List<DemoData>> groupBy = list.stream().collect(Collectors.groupingBy(DemoData::getBankName));

        List<FormBank> formBanks = new ArrayList<>();
        parentBanks.forEach(b->{
            if (groupBy.containsKey(b.getName())){
                List<DemoData> demoData = groupBy.get(b.getName());
                demoData.forEach(demoData1 -> {
                    FormBank formBank = new FormBank();
                    formBank.setPid(b.getId());
                    formBank.setArea(demoData1.getArea());
                    formBank.setName(demoData1.getFullName());
                    formBank.setShortName(demoData1.getShortName());
                    formBanks.add(formBank);
                });
            }
        });

        parentBanks.addAll(formBanks);

        parentBanks.forEach(System.out::println);

        LOGGER.info("存储数据库成功！");
    }

    private <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> concurrentHashMap = new ConcurrentHashMap<>();
        return t -> concurrentHashMap.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}