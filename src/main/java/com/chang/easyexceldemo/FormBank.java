package com.chang.easyexceldemo;

import lombok.Data;

import java.io.Serializable;

@Data
public class FormBank implements Serializable {
    private Integer id;
    private Integer pid;
    private String name;
    private String shortName;
    private String area;

}
